#!/bin/sh

# DESCRIPTION:
# This script helps with local development. 
# The script:
#  1. Cleans out the local conatainer and image store
#  2. Builds a new jenkins containter
#  3. Starts up jenkins via docker compose

docker rm $(docker ps -aq)
docker image rm $(docker image ls -aq)

docker build --tag jcasc:latest .
docker-compose up