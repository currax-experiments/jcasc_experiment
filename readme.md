# THIS IS AN IMPROGRESS EXPERIMENT .... USE AT OWN RISK!

## Using this experiment
1. You will need an sshkey named gitlab.priv 
2. You will need to build the jenkins container (e.g. docker build --tag jcasc:latest  )
3. You can launch using docker compose (docker-compose up )

## Things Discovered:
* Jobs defined in the jenkins.yml are not removed from Jenkins when removed from the yml
* Changes to jobs in the jenkins.yml are reflect in jenkins when change in the yml file
* JobDSL sets the "run as" user on the seed job. However, the JCasC plugin does not appear to have a configuration for the authorized-project plugin. The solve for this is to use the [post initialization script directory](https://wiki.jenkins.io/display/JENKINS/Post-initialization+script) and groovy to set configure the [Authorize Project plugin](https://wiki.jenkins.io/display/JENKINS/Authorize+Project+plugin). This allows the seed job to run unattended and not run into sandbox issues (because the sandbox is stoopid)

## Issues:
* __Queuing the Seed Job__ -  
     There is a race condition for the dsl job queue(), script security and JCasC. The workaround implemented in this example schedules the Seed job to run every 5 minutes. This resolves the issue for an example but is not a good solution for a production pipeline. 
     The issue is documented in the issue below. 
     Two potential workarounds:
     * Until this is resolved is to set a delay on the seed job run until one minute after the casc finishes.
     * Run the Seed job via the jenkins api after jenkins is up and running. 
  
     See [Issue 619](https://github.com/jenkinsci/configuration-as-code-plugin/issues/619)

## Resources
     Authorized Project:
     -------------------
     * URL: https://github.com/jenkinsci/authorize-project-plugin 
     * Notes: 
          Because reasons.

     Other Helpful resources:
     * https://github.com/jenkinsci/job-dsl-plugin/wiki/User-Power-Moves

## Files and Folders:
